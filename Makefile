CC = gcc
MPICC = mpicc
NVCC = nvcc
MPIRUN = mpirun
CFLAGS = -I. -g
PFLAGS = -fopenmp
OCVFLAGS = `pkg-config --cflags opencv`
LIBS = `cat test2.txt` #`pkg-config --libs opencv`
LIBS_N = `cat test3.txt` #`pkg-config --libs opencv`

all: serial parallel cuda
	$(CC) $(CFLAGS) $(OCVFLAGS) serial.o -o serial $(LIBS_N)
	$(MPICC) $(CFLAGS) $(PFLAGS) $(OCVFLAGS) parallel.o -o parallel $(LIBS_N) 
	$(NVCC) $(CFLAGS) $(OCVFLAGS) cuda.cu -o cuda $(LIBS)

cuda:
	$(NVCC) $(CFLAGS) $(OCVFLAGS) cuda.cu -o cuda $(LIBS)

run: parallel
	$(MPIRUN) -n 10 parallel ${ARGS}

serial:
	$(CC) $(CFLAGS) $(OCVFLAGS) -c serial.c

parallel:
	$(MPICC) $(PFLAGS) $(CFLAGS) $(OCVFLAGS) -c parallel.c

clean:
	rm -rf *.o serial parallel cuda testWorkload

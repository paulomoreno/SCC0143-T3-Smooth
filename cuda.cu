#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// OpenCV
#include <cv.h>
#include <highgui.h>

// OpenMP and OpenMPI
#include <omp.h>
#include <mpi.h>

#define TILE_DIM 14
#define C 3

double elapsed;

/* Kernel Function that calculates the smooth */
__global__ void smoothOnCuda (int *a, int *c, int W, int H){

	/* Getting the index  */
	int idx = threadIdx.x + blockDim.x * blockIdx.x;
	int idy = threadIdx.y + blockDim.y * blockIdx.y;
	int idz = threadIdx.z + blockDim.z * blockIdx.z;
	
	/* Other variables to be used */
	int x, y;
	int temp_result = 0;
	int count = 0;
	int index;

	/* Shared Memory */
	__shared__ int s_a[TILE_DIM+2][TILE_DIM+2][C];

	/* Offset calculation
		Here, we calculate a 2- offsset on the original image in
		order to get two extre columns and lines on the image
	 */
	int col = idx - 2;
	int row = idy - 2;

	/* Calculate the index of the current position (with the offset) on
		the original imagem array	
	*/
	index = (col + row*W)*C + idz;

	/* If this pixel exists on the original image, copy it to the shared memory */
	if(col >= 0 && col < W && row >= 0 && row < H){
		/* Copy the current pixel */
		s_a[threadIdx.x][threadIdx.y][threadIdx.z] = a[index];
		/* If we are on the right border, and considering the offset, copy two extra pixels on X */
		if (threadIdx.x == (TILE_DIM-1)){	
			for (x=1; x<=2; x++){
				s_a[threadIdx.x+x][threadIdx.y][threadIdx.z] = a[ (col+x + row*W)*C + idz ];
			}
		}
		/* If we are on the bottom border, and considering the offset, copy two extra pixels on Y */
		if (threadIdx.y == (TILE_DIM-1)){	
			for (y=1; y<=2; y++){
				s_a[threadIdx.x][threadIdx.y+y][threadIdx.z] = a[ (col + (row+y)*W)*C + idz ];
			}
		}
		/* If we are on the bottom right corner, and considering the offset, copy four extra pixels  */
	    if (threadIdx.x == (TILE_DIM-1) && threadIdx.y == (TILE_DIM-1)){	
			for (x=1; x<=2; x++){
				for (y=1; y<=2; y++){
					s_a[threadIdx.x+x][threadIdx.y+y][threadIdx.z] = a[ (col+x + (row+y)*W)*C + idz ];
				}
			}
		}

	}
	/* If this pixel doesnt exist, set the shared memory as 0*/
	else{
		s_a[threadIdx.x][threadIdx.y][threadIdx.z] = 0;
	}

	/* Wait for all other threads to finish copying data to the shared memory */
	__syncthreads();

	/* After the image partition is on the shared memory, all threads start calculating the smooth filter.
		  So first, we check if the current pixel belongs to the image:
	 */
	if(col >= 0 && col < W && row >= 0 && row < H){

		/* Calculate the smooth boundary - two pixels on each direction, giving us a 5x5 square */
		int leftLimitX = (int) threadIdx.x - 2;
		int rightLimitX = (int) threadIdx.x + 2;
		int leftLimitY = (int) threadIdx.y - 2;
		int rightLimitY = (int) threadIdx.y + 2;

		/* Extra variables */
		int ofx, ofy, ofInd;

		/* Main Loop - runs over all pixels on the 5x5 square */
		for(x = leftLimitX; x <= rightLimitX; x++){ // height
			for(y = leftLimitY; y <= rightLimitY; y++){ // width
				/* If the current pixel inside the 5x5 square is inside the shared memory matrix, 
					add it to the filter counter  */
				if(!((x)< 0 || (y) < 0 || x >= (TILE_DIM+2) || y >= (TILE_DIM+2) || 
					(x - 2) >= W || (y - 2) >= H )){
					temp_result += s_a[x][y][idz];
					count++;
				/* If the current pixel inside the 5x5 square is not inside the shared memory matrix, 
					but is still inside the image limits, get the data from the original array.

					this is necessary because even with the offset and bigger shared memory, we still
					need to get 2 extra pixels (two extra lines, and two extra columsn) on the left and top border  */
				}else if ( (x < 0 ) || (y < 0) ){

					/* For this to work, we calculate where this pixel would be on the original array,
						and then, if this index is inside the array, then this pixel does exist 
						and it means we should use it on the calculate */
					ofx = x - leftLimitX - 2;
					ofy = y - leftLimitY - 2;
					ofInd = (col+ ofx + (row+ofy)*W)*C + idz;
					if (ofInd >=0){
						temp_result += a[ofInd];
						count++;
					}
				}
			}	
		}	

		/* Wait for everyone to finish calculating and save the information on the return matrix*/
		__syncthreads();

		c[index] = (int) temp_result/count;

	}
}


/* Smooth function that receives an OpenCV image and does all the necessary preparation and then calculate the
 smooth filter on the GPU */
void smooth (IplImage* img){

	/* Get Image information*/
	int W = img->width;
	int H = img->height;

	/* Calculate the image size */
	size_t size = W*H*sizeof(int)*C;
	int num_thread, num_block_w, num_block_h;

	/* Create and allocate Host arrays */
	int *h_a, *h_c;
	h_a = (int*)malloc(size);
	h_c = (int*)malloc(size);

	/* Create and allocate GPU arrays*/
	int *d_a, *d_c;
	cudaMalloc(&d_a, size);
	cudaMalloc(&d_c, size);

	/* Copy data form OpenCV image to int array */
	int i = 0;
	for (i = 0; i < W*H*C; i++){
		h_a[i] = (unsigned char) img->imageData[i];
	}

	/* BEginning of CUDA Code*/

	/* Calculate GPU grid size - considering our 2 pixels offset on the image, explained on the kernel function  */
	num_thread = TILE_DIM;
	num_block_w = ceil((float)(W+2)/TILE_DIM);
	num_block_h = ceil((float)(H+2)/TILE_DIM);

	/* Copy image from Host to GPU */
	cudaMemcpy(d_a, h_a, size, cudaMemcpyHostToDevice);
	cudaMemset(d_c, 0, size);

	/* Set grid size and */
	dim3 gridsize(num_block_w,num_block_h,1);
	dim3 blocksize(num_thread,num_thread,3);

	/* Call Kernel function to calculate the smooth filter */
	smoothOnCuda<<<gridsize,blocksize>>>(d_a,d_c, W, H);
	/* Waits for GPU to finish calculating */
	cudaDeviceSynchronize();
	/* Copy image data from GPU to Host */
	cudaMemcpy(h_c, d_c, size, cudaMemcpyDeviceToHost);

	/* Copy image data from int array to OpenCV Image */
	for (i = 0; i < W*H*C; i++){
		img->imageData[i] = (unsigned char) h_c[i];
	}

	/* Deallocate the arrays */
	free(h_a);
	free(h_c);

	cudaFree(d_a);
	cudaFree(d_c);
}

/* The main function calculates the smooth filter 10 times and prints the times for each time */
int main (int argc, char* argv[]){

	/* Variable declaration */
	IplImage* img = 0; 
        struct timespec start, finish;
        double elapsed_temp;
	int i;

	/* Check for correct usage */
	if(argc<2){
		printf("Usage: main <image-file-name>\n\7");
		exit(0);
	}

	/* Load the desired image */  
	img=cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
	if(!img){
		printf("Could not load image file: %s\n",argv[1]);
		exit(0);
	}


	/* Main Loop - calculate the smooth filter 10 times */
	elapsed = 0;
	for(i = 0; i < 10; i++){
		//printf("Iteration %d of 10\n", i + 1); 

		/* Get time before calculating*/
		clock_gettime(CLOCK_MONOTONIC, &start);

		/* Calls the smooth function */
		smooth(img);

		/* Get the time after calculating, and convert it to seconds */
        	clock_gettime(CLOCK_MONOTONIC, &finish);
        	elapsed_temp = (finish.tv_sec - start.tv_sec);
        	elapsed_temp += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
	        elapsed += elapsed_temp;
		/* Prints elapsed time for this execution */
		printf("%.2lf\n", elapsed_temp);
	}

	/* Calculate Average execution time, and prints it */
	elapsed = (double) elapsed/10;
	printf("Average time of 10 calculations: %.2lfs\n", elapsed);

	/* Save resulting image */
	cvSaveImage("smoothCudaResult.jpg", img, 0);

	return 0;
}
